using Microsoft.AspNetCore.Mvc;
using Municorn.TestSolution.Api.Interfaces;
using Municorn.TestSolution.Core.Models.Requests;
using Municorn.TestSolution.Core.Models.Responses;

namespace Municorn.TestSolution.Api.Host.Controllers;

/// <summary>
/// Контроллер для отправки нотификаций и получения их состояния
/// </summary>
[Route("api/v2/[controller]/[action]")]
public class NotificationController : Controller
{
    private readonly IPushService _pushService;
    
    /// <summary>
    /// .ctor
    /// </summary>
    public NotificationController(IPushService pushService)
    {
        _pushService = pushService ?? throw new ArgumentNullException(nameof(pushService));
    }
    
    /// <summary>
    /// Метод отправки андроид сообщений
    /// </summary>
    [HttpPut]
    [ProducesResponseType(200, Type = typeof(PushNotificationResult))]
    [ProducesResponseType(400, Type = typeof(string))]
    public async Task<IActionResult> PushAndroidNotificationAsync([FromBody] AndroidNotification notification)
    {
        try
        {
            return Ok(await _pushService.PushNotificationAsync(notification));
        }
        catch (ArgumentException e)
        {
            return BadRequest(e.Message);
        }
    }
    
    /// <summary>
    /// Метод отправки ios сообщений
    /// </summary>
    [HttpPut]
    [ProducesResponseType(200, Type = typeof(PushNotificationResult))]
    [ProducesResponseType(400, Type = typeof(string))]
    public async Task<IActionResult> PushIOSNotificationAsync([FromBody] IOSNotification notification)
    {
        try
        {
            return Ok(await _pushService.PushNotificationAsync(notification));
        }
        catch (ArgumentException e)
        {
            return BadRequest(e.Message);
        }
    }

    /// <summary>
    /// Метод получения статуса отправки сообщений
    /// </summary>
    [HttpGet]
    [ProducesResponseType(200, Type = typeof(PushNotificationResult))]
    [ProducesResponseType(400, Type = typeof(string))]
    public async Task<IActionResult> NotificationStatus(Guid notificationId)
    {
        if (notificationId == Guid.Empty)
        {
            return BadRequest("Некорректный идентификатор уведомления");
        }

        try
        {
            return Ok(await _pushService.GetStatusNotificationAsync(notificationId));
        }
        catch (KeyNotFoundException e)
        {
            return NotFound();
        }
    }
}