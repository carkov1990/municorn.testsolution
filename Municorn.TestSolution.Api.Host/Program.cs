using Municorn.TestSolution.Api;
using Municorn.TestSolution.Api.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddSingleton<AndroidNotificationSender>();
builder.Services.AddSingleton<IOSNotificationSender>();
builder.Services.AddSingleton<IPushService, PushService>();
builder.Services.AddSingleton<INotificationSenderFactory, NotificationSenderFactory>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();

app.Run();