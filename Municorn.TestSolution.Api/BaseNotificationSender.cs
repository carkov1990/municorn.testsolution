using Municorn.TestSolution.Api.Interfaces;
using Municorn.TestSolution.Core.Models.Enum;
using Municorn.TestSolution.Core.Models.Requests;
using Municorn.TestSolution.Core.Models.Responses;

namespace Municorn.TestSolution.Api;

public abstract class BaseNotificationSender : INotificationSender
{
    private static Random _random = new Random();
    
    public virtual async Task<PushNotificationResult> SendAsync(INotification notification)
    {
        var delay = _random.Next(500, 2000);
        await Task.Delay(delay);
        Console.WriteLine(notification.ToString());
        return new PushNotificationResult()
        {
            NotificationId = Guid.NewGuid(),
            Status = NotificationResultEnum.Success
        };
    }
}