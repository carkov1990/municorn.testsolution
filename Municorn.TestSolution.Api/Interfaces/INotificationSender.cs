using Municorn.TestSolution.Core.Models.Requests;
using Municorn.TestSolution.Core.Models.Responses;

namespace Municorn.TestSolution.Api.Interfaces;

/// <summary>
/// Интерфейс отправителя уведомлений
/// </summary>
public interface INotificationSender
{
    /// <summary>
    /// Метод для отправки уведомлений
    /// </summary>
    Task<PushNotificationResult> SendAsync(INotification notification);
}