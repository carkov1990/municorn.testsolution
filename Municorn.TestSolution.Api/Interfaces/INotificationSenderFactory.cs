using Municorn.TestSolution.Core.Models.Requests;

namespace Municorn.TestSolution.Api.Interfaces;

/// <summary>
/// Интерфейс фабрики создания отправителя уведомлений
/// </summary>
public interface INotificationSenderFactory
{
    /// <summary>
    /// Метод создания сервиса отправителя уведомлений
    /// </summary>
    INotificationSender CreateNotificationSender<TNotification>()
        where TNotification : BaseNotification;
}