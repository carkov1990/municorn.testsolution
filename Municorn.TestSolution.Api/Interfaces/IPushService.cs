using Municorn.TestSolution.Core.Models.Requests;
using Municorn.TestSolution.Core.Models.Responses;

namespace Municorn.TestSolution.Api.Interfaces;

/// <summary>
/// Интерфейс сервиса отправки сообщений
/// </summary>
public interface IPushService
{
    /// <summary>
    /// Метод отправки и сохранения уведомлений
    /// </summary>
    Task<PushNotificationResult> PushNotificationAsync<TNotification>(TNotification notification)
        where TNotification : BaseNotification;

    /// <summary>
    /// Метод получения статусов отправки уведомлений
    /// </summary>
    Task<PushNotificationResult> GetStatusNotificationAsync(Guid notificationId);
}