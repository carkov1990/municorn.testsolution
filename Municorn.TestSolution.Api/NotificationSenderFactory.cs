using Municorn.TestSolution.Api.Interfaces;
using Municorn.TestSolution.Core.Models.Requests;

namespace Municorn.TestSolution.Api;

/// <summary>
/// Класс фабрики создания отправителя уведомлений
/// </summary>
public class NotificationSenderFactory : INotificationSenderFactory
{
    private readonly IServiceProvider _provider;
    
    /// <summary>
    /// .ctor
    /// </summary>
    public NotificationSenderFactory(IServiceProvider provider)
    {
        _provider = provider ?? throw new ArgumentNullException(nameof(provider));
    }
    
    /// <inheritdoc cref="INotificationSenderFactory.CreateNotificationSender{TNotification}"/>
    public INotificationSender CreateNotificationSender<TNotification>() where TNotification : BaseNotification
    {
        var sender = typeof(TNotification) switch
        {
            var _ when typeof(TNotification) == typeof(AndroidNotification) => _provider.GetService(
                typeof(AndroidNotificationSender)),
            var _ when typeof(TNotification) == typeof(IOSNotification) => _provider.GetService(
                typeof(IOSNotificationSender)),
            _ => throw new ArgumentException(nameof(TNotification))
        };
        return (INotificationSender) sender;
    }
}