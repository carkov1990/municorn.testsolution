﻿using System.Collections.Concurrent;
using Municorn.TestSolution.Api.Interfaces;
using Municorn.TestSolution.Core.Models.Enum;
using Municorn.TestSolution.Core.Models.Requests;
using Municorn.TestSolution.Core.Models.Responses;

namespace Municorn.TestSolution.Api;

/// <summary>
/// Класс отправки сообщений
/// </summary>
public class PushService : IPushService
{
    private readonly INotificationSenderFactory _notificationFactory;

    private static readonly ConcurrentDictionary<Guid, NotificationResultEnum> Storage = new ();

    public PushService(INotificationSenderFactory notificationFactory)
    {
        _notificationFactory = notificationFactory ?? throw new ArgumentNullException(nameof(notificationFactory));
    }

    /// <inheritdoc cref="IPushService.PushNotificationAsync{TNotification}"/>
    public async Task<PushNotificationResult> PushNotificationAsync<TNotification>(TNotification notification)
        where TNotification : BaseNotification
    {
        var validationResult = notification?.Validate();
        if (validationResult != null && validationResult.Value.IsValid)
        {
            var sender = _notificationFactory.CreateNotificationSender<TNotification>();
            var result = await sender.SendAsync(notification);
            Storage.AddOrUpdate(result.NotificationId, (id) => result.Status, (id, status) => result.Status);
            return result;
        }
        else
        {
            var errorMessages = validationResult.HasValue
                ? validationResult.Value.ErrorMessages
                : new[] {"Отсутсвует модель запроса."};
            throw new ArgumentException(String.Join('\n', errorMessages));
        }
    }

    /// <inheritdoc cref="IPushService.GetStatusNotificationAsync"/>
    public Task<PushNotificationResult> GetStatusNotificationAsync(Guid notificationId)
    {
        return Task.FromResult(new PushNotificationResult
        {
            NotificationId   = notificationId,
            Status = Storage[notificationId]
        });
    }
}