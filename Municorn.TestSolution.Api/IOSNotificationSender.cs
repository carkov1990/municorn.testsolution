using Municorn.TestSolution.Core.Models.Enum;
using Municorn.TestSolution.Core.Models.Requests;
using Municorn.TestSolution.Core.Models.Responses;

namespace Municorn.TestSolution.Api;

public class IOSNotificationSender : BaseNotificationSender
{
    private static SemaphoreSlim locker = new (1, 1);
    private static long _notificationNumber;
    
    public override async Task<PushNotificationResult> SendAsync(INotification notification)
    {
        var result = await base.SendAsync(notification);
        if (await locker.WaitAsync(0))
        {
            try
            {
                _notificationNumber++;
                if (_notificationNumber % 5 == 0)
                {
                    result.Status = NotificationResultEnum.Failed;
                }
            }
            finally
            {
                locker.Release();
            }
        }

        return result;
    }
}