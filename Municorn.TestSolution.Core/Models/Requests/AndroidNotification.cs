﻿namespace Municorn.TestSolution.Core.Models.Requests;

/// <summary>
/// Модель уведомления для Android
/// </summary>
public class AndroidNotification : BaseNotification
{
    /// <summary>
    /// строка до 50 символов. Уникальный идентификатор девайса, куда будет отправлено уведомление. Поле обязательное.
    /// </summary>
    public string DeviceToken { get; set; }

    /// <summary>
    /// строка до 2000 символов. Само сообщение. Поле обязательное.
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// строка до 255 символов. Поле обязательное.
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// строка до 2000 символов. Поле является опциональным.
    /// </summary>
    public string Condition { get; set; }

    /// <inheritdoc cref="BaseNotification.Validate"/>
    public override (bool IsValid, string[] ErrorMessages) Validate()
    {
        var errors = new List<string>();
        if (String.IsNullOrWhiteSpace(DeviceToken))
        {
            errors.Add($"Поле {nameof(DeviceToken)} не заполнено.");
        }
        else if (DeviceToken.Length > 50)
        {
            errors.Add($"Поле {nameof(DeviceToken)} имеет длину более 50 символов.");
        }

        if (String.IsNullOrWhiteSpace(Message))
        {
            errors.Add($"Поле {nameof(Message)} не заполнено.");
        }
        else if (Message.Length > 2000)
        {
            errors.Add($"Поле {nameof(Message)} имеет длину более 2000 символов.");
        }

        if (String.IsNullOrWhiteSpace(Title))
        {
            errors.Add($"Поле {nameof(Title)} не заполнено.");
        }
        else if (Title.Length > 255)
        {
            errors.Add($"Поле {nameof(Title)} имеет длину более 255 символов.");
        }

        if (Condition?.Length > 2000)
        {
            errors.Add($"Поле {nameof(Condition)} имеет длину более 2000 символов.");
        }

        return (!errors.Any(), errors.ToArray());
    }

    public override string ToString()
    {
        try
        {
            return $@"Type:{nameof(AndroidNotification)}. 
DeviceToken: {DeviceToken}.
Message : {Message}. 
Title : {Title}
Condition : {Condition}";
        }
        catch (Exception e)
        {
            return base.ToString();
        }
    }
}