namespace Municorn.TestSolution.Core.Models.Requests;

/// <summary>
/// Модель уведомления для IOS
/// </summary>
public class IOSNotification : BaseNotification
{
    /// <summary>
    ///  строка до 50 символов. Уникальный идентификатор девайса, куда будет отправлено уведомление. Поле обязательное.
    /// </summary>
    public string PushToken { get; set; }

    /// <summary>
    ///  строка до 2000 символов. Само сообщение. Поле обязательное.
    /// </summary>
    public string Alert { get; set; }

    /// <summary>
    /// целое число. По умолчанию должно принимать значение 10.
    /// </summary>
    public int Priority { get; set; } = 10;

    /// <summary>
    /// булево значение. По умолчанию должно принимать значение true.
    /// </summary>
    public bool IsBackground { get; set; } = true;

    /// <inheritdoc cref="BaseNotification.Validate"/>
    public override (bool IsValid, string[] ErrorMessages) Validate()
    {
        var errors = new List<string>();
        if (String.IsNullOrWhiteSpace(PushToken))
        {
            errors.Add($"Поле {nameof(PushToken)} не заполнено.");
        }
        else if (PushToken.Length > 50)
        {
            errors.Add($"Поле {nameof(PushToken)} имеет длину более 50 символов.");
        }

        if (String.IsNullOrWhiteSpace(Alert))
        {
            errors.Add($"Поле {nameof(Alert)} не заполнено.");
        }
        else if (Alert.Length > 2000)
        {
            errors.Add($"Поле {nameof(Alert)} имеет длину более 2000 символов.");
        }

        return (!errors.Any(), errors.ToArray());
    }

    public override string ToString()
    {
        try
        {
            return $@"Type:{nameof(IOSNotification)}. 
PushToken: {PushToken}.
Alert : {Alert}. 
Priority : {Priority}
IsBackground : {IsBackground}";
        }
        catch (Exception e)
        {
            return base.ToString();
        }
    }
}