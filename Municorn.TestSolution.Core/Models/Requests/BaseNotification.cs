namespace Municorn.TestSolution.Core.Models.Requests;

public abstract class BaseNotification : INotification
{
    public abstract (bool IsValid, string[] ErrorMessages) Validate();
}