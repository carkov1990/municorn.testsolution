namespace Municorn.TestSolution.Core.Models.Enum;

/// <summary>
/// Результат отправки уведомления
/// </summary>
public enum NotificationResultEnum
{
    /// <summary>
    /// Успешная отправка
    /// </summary>
    Success,
    
    /// <summary>
    /// Ошибка отправки
    /// </summary>
    Failed
}