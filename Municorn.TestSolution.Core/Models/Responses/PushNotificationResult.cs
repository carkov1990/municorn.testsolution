using Municorn.TestSolution.Core.Models.Enum;

namespace Municorn.TestSolution.Core.Models.Responses;

/// <summary>
/// Результат отправки уведомления
/// </summary>
public class PushNotificationResult
{
    /// <summary>
    /// Идентификатор уведомления
    /// </summary>
    public Guid NotificationId { get; set; }

    /// <summary>
    /// Статус отправки
    /// </summary>
    public NotificationResultEnum Status { get; set; }
}